FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += " \
               file://0001-verifiers-Don-t-return-error-for-deferred-image.patch \
               file://0002-grub-arm-fix-check-for-float-point.patch "

include grub_armv7_float.inc
